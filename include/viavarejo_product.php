<?php
class viaVarejo_product
{
  public function __construct($api_client,$rest_client)
  {
    $this->api_client = $api_client;
    $this->rest_client = $rest_client;
  }

  public function viaVarejo_getProducts()
  {
    $seller_items_Api = new \CNovaApiLojistaV2\SellerItemsApi($this->api_client);

    try {
        $get_seller_items_response = $seller_items_Api->getSellerItems('EX', 0, 100);
        return $get_seller_items_response;
    } catch (ApiException $e) {
        echo ($e->getMessage());
    }
  }

  public function viaVarejo_getProdutsSku()
  {
    $products = $this->viaVarejo_getProducts();

    foreach ($products->seller_items as $key => $value) {
      if($value['status'][0]->active == 'true') $sku_products[] = $value->sku_seller_id;
    }
    return $sku_products;
  }

  public function viaVarejo_updateProductPrice($sku,$price)
  {
    $seller_items_api = new \CNovaApiLojistaV2\SellerItemsApi($this->api_client);

    try {
      $prices = new CNovaApiLojistaV2\model\Prices();
      $prices->default = $price;
      $prices->offer = $price;
      /* return NULL when success*/
      return $seller_items_api->putSellerItemPrices($sku, $prices);
    } catch (ApiException $e) {
      echo ($e->getMessage());
    }

  }

  public function viaVarejo_updatePendingProducts($param)
  {
    $loads_api = new CNovaApiLojistaV2\LoadsApi($this->api_client);
    $product = new CNovaApiLojistaV2\model\Product();
    $product->sku_seller_id = $param['sku'];
    $product->product_seller_id;
    $product->title = $param['title'];
    $product->description = $param['description'];
    $product->brand = $param['brand'];
    $product->gtin;
    $product->categories = array(
      $param['category']
    );
    $product->images = array(
      $param['images'][0]
    );

    $price = new CNovaApiLojistaV2\model\ProductLoadPrices();
    $price->default = $param['price'];
    $price->offer = $param['price'];

    $product->price = $price;

    $stock = new CNovaApiLojistaV2\model\ProductLoadStock();
    $stock->quantity = $param['qty'];
    $stock->cross_docking_time = 0;

    $product->stock = $stock;

    $dimensions = new CNovaApiLojistaV2\model\Dimensions();
    $dimensions->weight = $param['weight'];
    $dimensions->length = $param['length'];
    $dimensions->width = $param['width'];
    $dimensions->height = $param['height'];

    $product->dimensions = $dimensions;

    $product_attr = new CNovaApiLojistaV2\model\ProductAttribute();
    $product_attr->name = '';
    $product_attr->value = '';

    $product->attributes =  array($product_attr);

    // Adiciona o novo produto na lista a ser enviada
    $products = array($product);
    try {
      var_dump($loads_api->putProduct($param['sku'],$products));

    } catch (ApiException $e) {
    	$errors = deserializeErrors($e->getResponseBody(), $this->api_client);
    	if ($errors != null) {
    		foreach ($errors->errors as $error) {
    			echo ($error->code . ' - ' . $error->message . "\n");
    		}
    	} else {
    		echo ($e->getMessage());
    	}
    }
  }

  public function viaVarejo_updateProductStock($sku,$qty)
  {
    $seller_items_api = new \CNovaApiLojistaV2\SellerItemsApi($this->api_client);

    try {
      $stock = new CNovaApiLojistaV2\model\Stock();
      $stock->quantity = $qty;
      $stock->cross_docking_time = 0;
      /* return NULL when success*/
      $seller_items_api->putSellerItemStock($sku, $stock);
    } catch (ApiException $e) {
      echo ($e->getMessage());
    }

  }

  public function viaVarejo_getPendingProducts()
  {
    $loads_api = new CNovaApiLojistaV2\LoadsApi($this->api_client);

    try {
    	$get_products_response = $loads_api->getProducts(null, null, 0, 100);
      // var_dump($get_products_response);
      foreach ($get_products_response->skus as $key => $value) $waitaprove[] = $value->sku_seller['id'];

      return $waitaprove;

    } catch (ApiException $e) {
    	$errors = deserializeErrors($e->getResponseBody(), $this->api_client);
    	if ($errors != null) {
    		foreach ($errors->errors as $error) {
    			echo ($error->code . ' - ' . $error->message . "\n");
    		}
    	} else {
    		echo ($e->getMessage());
    	}
    }
  }

  public function viaVarejoUpdateProductStatus($sku,$status)
  {
    $loads = new CNovaApiLojistaV2\LoadsApi($this->api_client);

    try {
      $sellerItemStatus = new CNovaApiLojistaV2\model\SellerItemStatusUpdate();
      $sellerItemStatus->sku_seller_id = $sku;
      $sellerItemStatus->status = array(
        array('active'=>$status,'site' => 'EX'),
        array('active'=>$status,'site' => 'CB'),
        array('active'=>$status,'site' => 'PF'),
        array('active'=>$status,'site' => 'CD'),
        array('active'=>$status,'site' => 'BT'),
        array('active'=>$status,'site' => 'HP'),
        array('active'=>$status,'site' => 'LA'),
        array('active'=>$status,'site' => 'PA'),
        array('active'=>$status,'site' => 'SF'));
      /* return NULL when success*/
      $sellerItemStatus = array($sellerItemStatus);
      // var_dump($sellerItemStatus);
      $loads->putSellerItemsStatus($sellerItemStatus);
    } catch (ApiException $e) {
      echo ($e->getMessage());
    }
  }

  public function viaVarejo_CreateProduct($param)
  {

    $loads = new CNovaApiLojistaV2\LoadsApi($this->api_client);

    // Criação de um novo produto

    $product = new CNovaApiLojistaV2\model\Product();

    $product->sku_seller_id = $param['sku'];
    $product->product_seller_id;
    $product->title = $param['title'];
    $product->description = $param['description'];
    $product->brand = $param['brand'];
    $product->gtin;
    $product->categories = array(
      $param['category']
    );
    $product->images = array(
      $param['images'][0]
    );

    $price = new CNovaApiLojistaV2\model\ProductLoadPrices();
    $price->default = $param['price'];
    $price->offer = $param['price'];

    $product->price = $price;

    $stock = new CNovaApiLojistaV2\model\ProductLoadStock();
    $stock->quantity = $param['qty'];
    $stock->cross_docking_time = 0;

    $product->stock = $stock;

    $dimensions = new CNovaApiLojistaV2\model\Dimensions();
    $dimensions->weight = $param['weight'];
    $dimensions->length = $param['length'];
    $dimensions->width = $param['width'];
    $dimensions->height = $param['height'];

    $product->dimensions = $dimensions;

    $product_attr = new CNovaApiLojistaV2\model\ProductAttribute();
    $product_attr->name = '';
    $product_attr->value = '';

    $product->attributes =  array($product_attr);

    // Adiciona o novo produto na lista a ser enviada
    $products = array($product);

    try {
       // return $this->rest_client->post('loads/products',$products);
      // Envia a carga de produtos
      $loads->postProducts($products);
      return true;
    } catch (ApiException $e) {

      $errors = deserializeErrors($e->getResponseBody(), $api_client);

      if ($errors != null) {
        foreach ($errors->errors as $error) {
          echo ($error->code . ' - ' . $error->message . "\n");
        }
        return false;
      } else {
        echo $e->getMessage();
        return false;
      }
    }
  }
}
?>
