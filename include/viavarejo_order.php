<?php
class viaVarejo_order
{
  public function __construct($api_client)
  {
    $this->api_client = $api_client;
  }

  public function viaVarejo_getOrders()
  {
    $orders_api = new \CNovaApiLojistaV2\OrdersApi($this->api_client);

    try {
        $get_orders_response = $orders_api->getOrders(0, 100);
        return $get_orders_response;
    } catch (ApiException $e) {
        echo ($e->getMessage());
    }
  }

  public function viaVarejo_getOrdersId()
  {
    $get_orders_response = $this->viaVarejo_getOrders();

    foreach ($get_orders_response->orders as $key => $value) $orders_id[] = $value->order_id;

    return $orders_id;
  }
}
?>
